<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\UserAddress;
use App\PaymentInformation;
use App\PersonalInformation;
use App\PaymentResponseAPI;

class HomeController extends Controller
{
    //
    public function home()
    {
        if (Auth::check()) {
            $personalInformation = PersonalInformation::where('email',Auth::user()->email)->first();
            if($personalInformation == null){
                return redirect()->to('/step-one');
            }
            $userAddress = UserAddress::where('email',Auth::user()->email)->first();
            if($userAddress == null){
                return redirect()->to('/step-two');
            }
            $paymentInformation = PaymentInformation::where('email',Auth::user()->email)->first();
            $paymentInformationResponse = PaymentResponseAPI::where('email',Auth::user()->email)->first();
            if($paymentInformation == null || $paymentInformationResponse == null){
                return redirect()->to('/step-three');
            }
            return view('home.home', ['firstName'=>$personalInformation->first_name]);

        }else{
            return view('login.login');
        }
    }
}

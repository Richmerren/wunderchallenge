<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\UserAddress;
use App\PaymentInformation;
use App\PersonalInformation;
use App\PaymentResponseAPI;

class LoginController extends Controller
{
    //======================LOGIN==========================
   public function login(){
       return view('login.login');
   }
    public function userLogin(){
        $this->validate(request(),[
            'email'=>'required|email',
            'password'=>'required'
        ]);

        /**
         * Find if email is already registered
         * If not registered, register 
         * If registered, find state of register
         * redirect to /step-one
         */
        // $user = User::Create(request(['email','password']));

        $credentials = request(['email','password']);
        
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            $personalInformation = PersonalInformation::where('email',Auth::user()->email)->first();
            if($personalInformation == null){
                return redirect()->to('/step-one');
            }
            $userAddress = UserAddress::where('email',Auth::user()->email)->first();
            if($userAddress == null){
                return redirect()->to('/step-two');
            }
            $paymentInformation = PaymentInformation::where('email',Auth::user()->email)->first();
            $paymentInformationResponse = PaymentResponseAPI::where('email',Auth::user()->email)->first();
            if($paymentInformation == null || $paymentInformationResponse == null){
                return redirect()->to('/step-three');
            }
            return redirect()->to('/');
        }

    }

    public function userLogout(){
        Auth::logout();
        return redirect()->to('/');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;



use App\User;
use App\UserAddress;
use App\PaymentInformation;
use App\PersonalInformation;
use App\PaymentResponseAPI;

class RegistrationController extends Controller
{
    //======================REGISTER==========================
    // Show register page
    public function create()
    {
        return view('registration.create');
    }

    public function store(){
        $this->validate(request(),[
            'email'=>'required|email',
            'password'=>'required'
        ]);

        /**
         * Find if email is already registered
         * If not registered, register 
         * If registered, find state of register
         * redirect to /step-one
         */

        $user = User::Create(request(['email','password']));

        auth()->login($user);

        return redirect()->to('/step-one');
    }

    //======================STEP ONE==========================
    
    //Show addresses page
    public function stepOne(){
        return view('registration.step-one',['user'=>  Auth::user()->email]);
    }
    
    public function SavePersonalInfo(){
        $this->validate(request(),[
            'email'=>'required',
            'first_name'=>'required',
            'last_name'=>'required',
            'phone'=>'required'
            ]);

            $address = PersonalInformation::Create(request(['email','first_name','last_name','phone']));

            return redirect()->to('/step-two');
    }

    //======================STEP TWO==========================

    //Show addresses page
    public function stepTwo(){
        return view('registration.step-two',['user'=>  Auth::user()->email]);
    }

    public function saveAddress(){
        $this->validate(request(),[
            'email'=>'required',
            'street'=>'required',
            'house_number'=>'required',
            'zip_code'=>'required',
            'city'=>'required'
            ]);

            $address = UserAddress::Create(request(['email',
            'street',
            'house_number',
            'zip_code',
            'city']));

            return redirect()->to('/step-three');
    }

    //======================STEP THREE==========================

    public function stepThree(){
        return view('registration.step-three',['user'=>  Auth::user()->email]);
    }

    public function savePayment(){
        $this->validate(request(),[
            'email'=>'required',
            'account_number'=>'required',
            'iban'=>'required',
            ]);
            
            $userId = Auth::user()->id;
            $payment = PaymentInformation::Create(request(['email','account_number','iban']));
            $personalData = PersonalInformation::where('email', Auth::user()->email)->first();
            // dd($personalData);
            $fullName = $personalData->first_name .' '.$personalData->last_name;
            $iban = request('iban');
            // Call API
            $client = new Client(); //GuzzleHttp\Client
            $headers = ['Content-Type' => 'application/json'];
            $body = array('customerId' =>  $userId, 'iban' => $iban, 'owner' => $fullName);
            $jsonBody = json_encode($body);
    
            $guzzleRequest = $client->post('https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data', [
                'body'=> $jsonBody
            ]);
            
            if($guzzleRequest->getStatusCode() == 200){
                $stringBody = (string) $guzzleRequest->getBody();
                $jsonBody = json_decode($stringBody);
                // dd($jsonBody);
                $payment = PaymentResponseAPI::Create(['email'=>Auth::user()->email,'status'=> (string)$guzzleRequest->getStatusCode(),'paymentDataId'=> $jsonBody->paymentDataId]);
               
                return redirect()->to('/step-four');
            }
    }

    //======================STEP FOUR==========================

    public function stepFour(){

        $apiResponse = PaymentResponseAPI::where('email', Auth::user()->email)->first();
        // dd($apiResponse->paymentDataId);
        $personal = PersonalInformation::where('email', Auth::user()->email)->first();
        // dd($personal);

        return view('registration.step-four',['firstName'=> $personal->first_name ,'paymentId'=> $apiResponse->paymentDataId]);
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@home');

Route::get('/login', 'LoginController@login');
Route::post('userLogin', 'LoginController@userLogin');
Route::get('/logout', 'LoginController@userLogout');

Route::get('/register', 'RegistrationController@create');
Route::post('register', 'RegistrationController@store');

Route::get('/step-one', 'RegistrationController@stepOne');
Route::post('savePersonalInfo', 'RegistrationController@SavePersonalInfo');

Route::get('/step-two', 'RegistrationController@stepTwo');
Route::post('saveAddress', 'RegistrationController@saveAddress');

Route::get('/step-three', 'RegistrationController@stepThree');
Route::post('savePayment', 'RegistrationController@savePayment');

Route::get('/step-four', 'RegistrationController@stepFour');
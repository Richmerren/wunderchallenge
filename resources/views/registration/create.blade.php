@extends('layouts.master')
 
@section('content')
 
    <h2>Register</h2>
    <form method="POST" action="register">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="text" class="form-control" id="email" name="email">
        </div>
 
        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password" name="password">
        </div>
<!-- 
        <div class="form-group">
            <label for="passwordConfirm">Password:</label>
            <input type="passwordConfirm" class="form-control" id="passwordConfirm" name="passwordConfirm">
        </div> -->
 
        <div class="form-group">
            <button style="cursor:pointer" type="submit" class="btn btn-primary">Next</button>
        </div>
    </form>
 
@endsection
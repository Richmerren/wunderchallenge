@extends('layouts.master')
 
@section('content')
 
    <h2>Register - Payment Info</h2>
    <form method="POST" action="savePayment">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="email"></label>
            <input type="hidden" class="form-control" id="email" name="email" value={{$user}} >
        </div>
        <div class="form-group">
            <label for="account_number">Account Number:</label>
            <input type="text" class="form-control" id="account_number" name="account_number">
        </div>
        
        <div class="form-group">
            <label for="iban">IBAN:</label>
            <input type="text" class="form-control" id="iban" name="iban">
        </div>
        <div class="form-group">
            <button style="cursor:pointer" type="submit" class="btn btn-primary">Next</button>
        </div>
    </form>
    <a href='logout'>Logout</a>
 
@endsection
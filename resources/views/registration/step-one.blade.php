@extends('layouts.master')
 
@section('content')
 
    <h2>Register - Personal Information</h2>
    <form method="POST" action="savePersonalInfo">
        {{ csrf_field() }}
            <input type="hidden" class="form-control" id="email" name="email" value={{$user}} >
        
        <div class="form-group">
            <label for="first_name">First Name:</label>
            <input type="text" class="form-control" id="first_name" name="first_name">
        </div>

        <div class="form-group">
            <label for="last_name">Last Name:</label>
            <input type="text" class="form-control" id="last_name" name="last_name">
        </div>
 
        <div class="form-group">
            <label for="phone">Telephone:</label>
            <input type="phone" class="form-control" id="phone" name="phone">
        </div> 
        <div class="form-group">
            <button style="cursor:pointer" type="submit" class="btn btn-primary">Next</button>
        </div>
    </form>
    <a href='logout'>Logout</a>
 
@endsection
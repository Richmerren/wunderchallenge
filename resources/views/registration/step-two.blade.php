@extends('layouts.master')
 
@section('content')
 
    <h2>Register - Address</h2>
    <form method="POST" action="saveAddress">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="email"></label>
            <input type="hidden" class="form-control" id="email" name="email" value={{$user}} >
        </div>
        <div class="form-group">
            <label for="street">Street:</label>
            <input type="text" class="form-control" id="street" name="street">
        </div>
        
        <div class="form-group">
            <label for="house_number">House Number:</label>
            <input type="text" class="form-control" id="house_number" name="house_number">
        </div>
 
        <div class="form-group">
            <label for="zip_code">Zip Code:</label>
            <input type="text" class="form-control" id="zip_code" name="zip_code">
        </div>
 
        <div class="form-group">
            <label for="city">City:</label>
            <input type="text" class="form-control" id="city" name="city">
        </div>
 
        <div class="form-group">
            <button style="cursor:pointer" type="submit" class="btn btn-primary">Next</button>
        </div>
    </form>
    <a href='logout'>Logout</a>
 
@endsection
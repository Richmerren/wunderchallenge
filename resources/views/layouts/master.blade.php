<!-- Stored in resources/views/layouts/master.blade.php -->

<html>
    <head>
        <title>App Name - Wunder Challenge</title>
    </head>
    <body>
        @section('sidebar')
            This is the Wunder Challenge.
        @show

        <div class="container">
            @yield('content')
        </div>
    </body>
</html>
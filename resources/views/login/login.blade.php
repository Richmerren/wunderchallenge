@extends('layouts.master')
 
@section('content')
 
<h2>Login</h2>
    <form method="POST" action="userLogin">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="text" class="form-control" id="email" name="email">
        </div>
 
        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password" name="password">
        </div>
<!-- 
        <div class="form-group">
            <label for="passwordConfirm">Password:</label>
            <input type="passwordConfirm" class="form-control" id="passwordConfirm" name="passwordConfirm">
        </div> -->
 
        <div class="form-group">
            <button style="cursor:pointer" type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
        <a href='register'>Go to Register</a>
 
@endsection